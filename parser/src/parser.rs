use pest::{
    iterators::{Pair, Pairs},
    Parser,
};

use crate::{
    ast::{
        BinOpKind, Condition, ConditionKind, Literal, Modifier, Operation, Program,
        TopLevelDeclaration,
    },
    grammar::{Rule, SecsParser},
};

fn parse_push(elem: Pair<'_, Rule>) -> anyhow::Result<Operation> {
    let lit = match elem.as_rule() {
        Rule::number => Literal::Integer(elem.as_str().parse::<isize>()?),
        Rule::string => {
            let s = snailquote::unescape(elem.as_str())?;
            Literal::Str(s)
        }
        _ => unreachable!(),
    };
    Ok(Operation::Push(lit))
}
fn parse_modifier(elem: Pair<'_, Rule>) -> Modifier {
    match elem.as_str() {
        "." => Modifier::Preserve,
        "!" => Modifier::Destroy,
        _ => unreachable!(),
    }
}
fn parse_opkind(elem: Pair<'_, Rule>) -> BinOpKind {
    match elem.as_str() {
        "+" => BinOpKind::Add,
        "-" => BinOpKind::Sub,
        "*" => BinOpKind::Mul,
        "/" => BinOpKind::Div,
        "%" => BinOpKind::Rem,
        _ => unreachable!(),
    }
}
fn parse_condition(elem: Pair<'_, Rule>) -> anyhow::Result<Condition> {
    match elem.as_rule() {
        Rule::condition => {
            let mut childs = elem.into_inner();
            let cmpkind = match childs.next().unwrap().as_str() {
                "<" => ConditionKind::Lt,
                "<=" => ConditionKind::Le,
                "=" => ConditionKind::Eq,
                "!=" => ConditionKind::Ne,
                ">" => ConditionKind::Gt,
                ">=" => ConditionKind::Ge,
                _ => unreachable!(),
            };
            let modifier = parse_modifier(childs.next().unwrap());
            let second_member = if let Some(c) = childs.next() {
                Some(c.as_str().parse::<isize>()?)
            } else {
                None
            };
            Ok(Condition {
                cond: cmpkind,
                modifier,
                member: second_member,
            })
        }
        _ => unreachable!(),
    }
}
fn parse_block(body: Pairs<'_, Rule>) -> anyhow::Result<Vec<Operation>> {
    body.map(|op| match op.as_rule() {
        Rule::pop => Ok(Operation::Pop),
        Rule::swap => Ok(Operation::Swap),
        Rule::clone => Ok(Operation::Clone),
        Rule::increment => Ok(Operation::Increment),
        Rule::push => parse_push(op.into_inner().next().unwrap()),
        Rule::deref => {
            let m = parse_modifier(op.into_inner().next().unwrap());
            Ok(Operation::Deref(m))
        }
        Rule::load => {
            let mut childs = op.into_inner();

            let m = parse_modifier(childs.next().unwrap());
            let i = if let Some(c) = childs.next() {
                Some(c.as_str().parse::<usize>()?)
            } else {
                None
            };
            Ok(Operation::Load(m, i))
        }
        Rule::store => {
            let mut childs = op.into_inner();

            let m = parse_modifier(childs.next().unwrap());
            let i = if let Some(c) = childs.next() {
                Some(c.as_str().parse::<usize>()?)
            } else {
                None
            };
            Ok(Operation::Store(m, i))
        }
        Rule::raise => {
            let mut childs = op.into_inner();
            let i = childs.next().unwrap().as_str().parse::<usize>()?;
            Ok(Operation::Raise(i))
        }
        Rule::call => {
            let mut childs = op.into_inner();
            let modifier = parse_modifier(childs.next().unwrap());
            let nbargs = childs.next().unwrap().as_str().parse::<usize>()?;
            let fnname = childs.next().unwrap().as_str().to_string();
            Ok(Operation::FnCall(fnname, modifier, nbargs, false))
        }
        Rule::call_void => {
            let mut childs = op.into_inner();
            let modifier = parse_modifier(childs.next().unwrap());
            let nbargs = childs.next().unwrap().as_str().parse::<usize>()?;
            let fnname = childs.next().unwrap().as_str().to_string();
            Ok(Operation::FnCall(fnname, modifier, nbargs, true))
        }
        Rule::ifelse => {
            let mut childs = op.into_inner();
            let condition = parse_condition(childs.next().unwrap())?;
            let thenbody = parse_block(childs.next().unwrap().into_inner())?;
            let elsebody = parse_block(childs.next().unwrap().into_inner())?;
            Ok(Operation::IfElse(condition, thenbody, elsebody))
        }
        Rule::whileloop => {
            let mut childs = op.into_inner();
            let condition = parse_condition(childs.next().unwrap())?;
            let loopbody = parse_block(childs.next().unwrap().into_inner())?;
            Ok(Operation::Loop(condition, loopbody))
        }
        Rule::binop => {
            let mut childs = op.into_inner();
            let opkind = parse_opkind(childs.next().unwrap());
            let m = parse_modifier(childs.next().unwrap());
            Ok(Operation::BinOp(opkind, m))
        }
        Rule::global_load => {
            let name = op.into_inner().next().unwrap().as_str();
            Ok(Operation::LoadGlobal(name.to_owned()))
        }
        Rule::global_store => {
            let mut childs = op.into_inner();
            let m = parse_modifier(childs.next().unwrap());
            let name = childs.next().unwrap().as_str();
            Ok(Operation::StoreGlobal(name.to_owned(), m))
        }

        _ => unreachable!(),
    })
    .collect()
}
fn parse_fndecl(
    mut elems: Pairs<'_, Rule>,
    is_variadic: bool,
) -> anyhow::Result<TopLevelDeclaration> {
    let nbargs = elems.next().unwrap().as_str().parse::<usize>()?;
    let fnname = elems.next().unwrap().as_str().to_owned();
    Ok(TopLevelDeclaration::FunctionDecl(
        fnname,
        nbargs,
        is_variadic,
    ))
}
pub fn parse(source: &str) -> anyhow::Result<Program> {
    let f = SecsParser::parse(Rule::file, source)?.next().unwrap();
    let tlds = f
        .into_inner()
        .map(|tld| match tld.as_rule() {
            Rule::fndef => {
                let mut childs = tld.into_inner();
                let name = childs.next().unwrap().as_str().to_string();
                let body = childs.next().unwrap();
                let ops = parse_block(body.into_inner())?;
                Ok(TopLevelDeclaration::FunctionDef(name, ops))
            }
            Rule::fndecl => parse_fndecl(tld.into_inner(), false),
            Rule::vararg_fndecl => parse_fndecl(tld.into_inner(), true),
            Rule::global_decl => {
                let name = tld.into_inner().next().unwrap().as_str();
                Ok(TopLevelDeclaration::GlobalDecl(name.to_owned()))
            }
            _ => unreachable!(),
        })
        .collect::<anyhow::Result<Vec<_>>>()?;
    Ok(Program::new(tlds))
}
