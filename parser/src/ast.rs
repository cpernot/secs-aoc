#[derive(Debug, Eq, PartialEq)]
pub struct Program {
    pub items: Vec<TopLevelDeclaration>,
}
impl Program {
    pub fn new(items: Vec<TopLevelDeclaration>) -> Self {
        Self { items }
    }
}
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TopLevelDeclaration {
    FunctionDef(String, Vec<Operation>),
    FunctionDecl(String, usize, bool),
    GlobalDecl(String),
}
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Operation {
    BinOp(BinOpKind, Modifier),
    Clone,
    Deref(Modifier),
    FnCall(String, Modifier, usize, bool),
    IfElse(Condition, Vec<Operation>, Vec<Operation>),
    Increment,
    Load(Modifier, Option<usize>),
    Loop(Condition, Vec<Operation>),
    Pop,
    Push(Literal),
    Raise(usize),
    Store(Modifier, Option<usize>),
    Swap,
    LoadGlobal(String),
    StoreGlobal(String, Modifier),
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum BinOpKind {
    Add,
    Sub,
    Mul,
    Div,
    Rem,
}
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Condition {
    pub cond: ConditionKind,
    pub modifier: Modifier,
    pub member: Option<isize>,
}
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ConditionKind {
    Lt,
    Le,
    Eq,
    Ne,
    Gt,
    Ge,
}
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Modifier {
    Preserve, // Preserve args
    Destroy,  // Destroy args
}
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Literal {
    Str(String),
    Integer(isize),
}
