pub mod ast;
mod grammar;
mod parser;

pub use parser::parse;

#[cfg(test)]
mod tests {
    use crate::ast::{self, Condition, ConditionKind, Modifier, Operation, TopLevelDeclaration};
    use crate::parse;

    #[test]
    fn parse_test() {
        // Insanely wrong code, just to check the parser
        let p = parse(r#"$main{100,2+!<.200[idpc!1puts=.0?3:"HelloWorld\n";!!]}"#)
            .expect("Failed to parse input");
        let p_expected = ast::Program::new(
            [TopLevelDeclaration::FunctionDef(
                "main".to_owned(),
                [
                    Operation::Push(ast::Literal::Integer(100)),
                    Operation::Push(ast::Literal::Integer(2)),
                    Operation::BinOp(ast::BinOpKind::Add, Modifier::Destroy),
                    Operation::Loop(
                        Condition {
                            cond: ConditionKind::Lt,
                            modifier: Modifier::Preserve,
                            member: Some(200),
                        },
                        [
                            Operation::Increment,
                            Operation::Clone,
                            Operation::Pop,
                            Operation::FnCall("puts".to_owned(), Modifier::Destroy, 1, true),
                            Operation::IfElse(
                                Condition {
                                    cond: ConditionKind::Eq,
                                    modifier: Modifier::Preserve,
                                    member: Some(0),
                                },
                                [Operation::Push(ast::Literal::Integer(3))].into(),
                                [Operation::Push(ast::Literal::Str(
                                    "HelloWorld\n".to_owned(),
                                ))]
                                .into(),
                            ),
                            Operation::Deref(Modifier::Destroy),
                        ]
                        .into(),
                    ),
                ]
                .into(),
            )]
            .into(),
        );
        assert_eq!(p, p_expected);
    }
}
