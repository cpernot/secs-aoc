use secs_interpreter::interpreter::Interpreter;
use slint::ComponentHandle;
fn main() -> anyhow::Result<()> {
    let win = MainWindow::new()?;
    let win_weak = win.as_weak();
    win.on_run(move |code| {
        let w = win_weak.upgrade().unwrap();
        let mut i = match Interpreter::new(&code) {
            Ok(i) => i,
            Err(err) => {
                w.set_stderr(format!("Impossible d'initialiser l'interpréteur: {}", err).into());
                w.set_stdout("".into());
                return;
            }
        };
        let res = i.run_timeout(1000);
        match res.error {
            Some(err) => {
                w.set_stderr(err.into());
                w.set_stdout(res.stdout.into());
            }
            None => {
                w.set_stderr("".into());
                w.set_stdout(res.stdout.into());
            }
        }
    });
    win.set_help_text(include_str!("../help.txt").into());
    win.run()?;
    Ok(())
}

slint::slint! {
    import { Button, CheckBox, HorizontalBox, Palette, ScrollView, VerticalBox } from "std-widgets.slint";
    component ShowText inherits Rectangle{
        in-out property text <=> t.text;
        background: Palette.alternate-background;
        t := Text{
            x: 0px;
            y: 0px;
            width: parent.width;
            height: parent.width;
            font-family: "monospace";
            font-size: 16px;
            wrap: word-wrap;
        }
    }
    component TextBox{
        in-out property text <=> inner.text;
        in property label <=> l.text;
        VerticalBox{
            l := Text {
                font-size: 16px;
                wrap: no-wrap;
            }
            inner := ShowText {
            }
        }
    }
    component Editor inherits Rectangle {
        in-out property text <=> ti.text;
        background: Palette.alternate-background;
        ti := TextInput{
            x: 0px;
            y: 0px;
            font-family: "monospace";
            font-size: 16px;
            single-line: false;
            wrap: word-wrap;
        }
    }
    component Control inherits Rectangle{
        callback run <=> btn.clicked;
        callback show_help <=> help.clicked;
        HorizontalBox{
            btn := Button {
                text: "Run program";
                primary: true;
            }
            help := Button{
                text: "Show help";
            }
        }
    }
    component ShowHelp inherits Text{
        font-size: 16px;
        font-family: "monospace";
        wrap: word-wrap;
    }
    export component MainWindow inherits Window {
        callback run(string);
        out property code <=> editor.text;
        in property <string> help_text;
        in property <string> stdout;
        in property <string> stderr;
        HorizontalLayout{
            x: 10px;
            y: 10px;
            width: (root.width) - 20px;
            height: (parent.height) - 20px;
            editor := Editor{
                horizontal-stretch: 1;
                text: "$main{\"Hello world\"c!1print_string}";
            }
            VerticalLayout{
                horizontal-stretch: 0;
                ctrl := Control{
                    vertical-stretch: 0;
                    run => {
                        run(code);
                    }
                    show_help => {
                        help_popup.show();
                    }
                }
                TextBox{
                    vertical-stretch: 1;
                    text: stdout;
                    label: "Program output:";
                }
                TextBox{
                    vertical-stretch: 1;
                    text: stderr;
                    label: "Program errors:";
                }
            }
        }
        help_popup := PopupWindow{
            width: 800px;
            height: 600px;
            x: (editor.width / 2) + 20px - 400px;
            y: (editor.height / 2) + 20px - 300px;

            ScrollView{
                width: parent.width;
                height: parent.height;
                viewport-height: help_inner.height;
                Rectangle{
                    background: Palette.control-background;
                    help_inner:= ShowHelp{
                        x: 0px;
                        y: 0px;
                        text: help_text;
                    }
                }
            }
        }
    }
}
