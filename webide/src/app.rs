use std::rc::Rc;

use anyhow::anyhow;
use egui::{Button, Color32, FontId, RichText};
use rand::prelude::*;
use secs_interpreter::interpreter::{Interpreter, Runner};
const TAB_SIZE: usize = 10;
const HELP_TEXT: &str = r#"SECS - Aide

Dans cette aide, $n représente l'élément d'indice n à partir  du haut de la pile

Déclarations:
  &var        Variable globale initialisée à 0 (ou NULL)
  $fonction{} Définit une fonction
  #fonction   Déclare une fonction

Opérations basiques: (un . signifie que l'opération prend un modificateur)
  i   incrémenter (push(pop() + 1 ))
  p   pop()
  d   clone (push($0))
  30  push(30)
  "H" pousse un char* dans la pile
  s   échange les éléments en haut de pile
  (3) pousse en haut de la pile le 4ème élément en partant du haut
  L.  accède à $1[$0] avec $1 un pointeur et $0 un indice
  S.  stocke dans un tableau: $2[$1] = $0
  +.  fait une addition: push($0 + $1)
  !.  déréférence un pointeur: push(*$0)
  Gl  charge une variable globale (ex. Glvariable)
  Gs. stocke dans une variable globale (variable = $0)

Fonctions builtin:
  print_string(string) prend un argument
  print_int(int)
  print_stack()
  malloc(int) -> ptr
  free(ptr)

Modificateurs: 
 ! pour détruire les arguments de l'opération
 . pour les conserver

Appel de fonction:
  c.nbargsname Appel de `name` avec `nbargs` arguments
  C.nbargsname Pour conserver la valeur de retour sur la pile

Conditions:
  Symbole de comparaison, modificateur, (opérande optionnel)
  exemple:
    >.   teste $0 > $1
    >=.10 teste $0 >= 10

Boucles: conditon[opérations]
  exemple: <.10[i]

If-Else:
  condition?then:else;
    exemple: !=.0?p:i;
"#;
const HELP_PART1: &str = r#"Partie I: Tours de Hanoï
Le but est de déplacer une pile de 5 disques de taille croissante d'une position 0 à une position 2 en déplaçant un disque à la fois, sachant qu'on ne peut pas placer un disque sur un autre de taille inférieure.
On dispose pour cela d'une fonction builtin move: int -> int -> unit qui fait le déplacement et qui affiche le nouvel état des tours.
Exemple: 0,1c!2move déplace le palet en haut de la première tour sur la deuxième tour."#;
const HELP_PART2: &str = r#"Partie II: Tri de liste
On cherche à faire un tri de liste. On dispose pour cela des fonctions suivantes:
 get: indice -> valeur (la valeur est poussée directement sur la pile par la fonction)
 set: indice -> valeur -> unit
   Assigne la valeur donnée à la case 
 swap: indice -> indice -> unit
   Échange les éléments du tableau aux indices donnés
 verifier: unit -> uni
  Indique dans la sortie si la liste est triée ou non.
  Si elle ne l'est pas, affiche l'état initial du tableau et l'état actuel"#;
pub struct TemplateApp {
    codeinput: String,
    timeout_enabled: bool,
    timeout_value: u64,
    program_output: String,
    program_errors: String,
    show_editor: bool,
    show_output: bool,
    show_errors: bool,
    show_runoptions: bool,
    show_grammar: bool,
    show_help: bool,
    show_part1: bool,
    show_part2: bool,
}

impl Default for TemplateApp {
    fn default() -> Self {
        Self {
            codeinput: r#"$main{"Hello World"c!1print_string}"#.into(),
            program_errors: String::new(),
            program_output: String::new(),
            timeout_enabled: true,
            timeout_value: 1000,
            show_editor: true,
            show_output: true,
            show_errors: true,
            show_runoptions: false,
            show_grammar: false,
            show_help: false,
            show_part1: false,
            show_part2: false,
        }
    }
}

impl TemplateApp {
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        Default::default()
    }
    fn run_user_code(&mut self) {
        let timeout = if self.timeout_enabled {
            self.timeout_value
        } else {
            0
        };
        let mut interpreter = match Interpreter::new(&self.codeinput) {
            Ok(i) => i,
            Err(e) => {
                self.program_output.clear();
                self.program_errors = e.to_string();
                return;
            }
        };
        // Need a RefCell here to keep mv Fn and not FnMut
        let tours = std::cell::RefCell::new([[5, 4, 3, 2, 1].to_vec(), [].to_vec(), [].to_vec()]);
        let tableau_orig = loop {
            let t = random_vec(TAB_SIZE);
            if !t.is_sorted() {
                break t;
            }
        };
        let tableau = Rc::new(std::cell::RefCell::new(tableau_orig.clone()));
        let tableau_clone = Rc::clone(&tableau);
        // Définition des fonctions builtin
        let swap = move |r: &mut Runner<'_>, args: Vec<usize>| {
            if args.len() != 2 {
                return Err(anyhow!("Expected 2 args for `swap`"));
            }
            if args[0] >= TAB_SIZE || args[1] >= TAB_SIZE {
                return Err(anyhow!("`swap`: index out of bounds"));
            }
            let mut tableau_ref = tableau_clone.borrow_mut();
            r.print(&format!("swap({},{})\n", args[0], args[1]))?;
            r.print(&format!("Avant: {:?}\n", tableau_ref))?;
            tableau_ref.swap(args[0], args[1]);
            r.print(&format!("Après: {:?}\n", tableau_ref))?;
            Ok(0)
        };
        let tableau_clone = Rc::clone(&tableau);
        let get = move |r: &mut Runner<'_>, args: Vec<usize>| {
            if args.len() != 1 {
                return Err(anyhow!("Expected 1 arg for `get`"));
            }
            if args[0] >= TAB_SIZE {
                return Err(anyhow!("`get`: index out of bounds"));
            }
            r.push(tableau_clone.borrow()[args[0]]);
            Ok(0)
        };
        let tableau_clone = Rc::clone(&tableau);
        let set = move |_r: &mut Runner<'_>, args: Vec<usize>| {
            if args.len() != 2 {
                return Err(anyhow!("Expected 1 arg for `set`"));
            }
            if args[0] >= TAB_SIZE {
                return Err(anyhow!("`set`: index out of bounds"));
            }
            tableau_clone.borrow_mut()[args[0]] = args[1];
            Ok(0)
        };
        let verifier = move |r: &mut Runner<'_>, args: Vec<usize>| {
            if !args.is_empty() {
                return Err(anyhow!("Expected 0 arg for `verifier`"));
            }
            let tab = tableau.borrow();
            let mut expected = tableau_orig.clone();
            expected.sort();
            if expected == *tab {
                r.print("Le tableau est trié! Vous avez réussi la partie 2")?;
            } else {
                r.print(format!(
                    "Le tableau n'est pas trié!\nValeur intiale: {:?}\nValeur finale: {:?}",
                    tableau_orig, *tab
                ))?;
            }
            Ok(0)
        };
        let mv = move |r: &mut Runner<'_>, args: Vec<usize>| {
            if args.len() != 2 {
                anyhow::bail!("Expected 2 args for move");
            }
            let (i1, i2) = (args[0], args[1]);
            let mut t = tours.borrow_mut();
            if i1 >= 3 || i2 >= 3 {
                anyhow::bail!("Move: index out of bounds");
            }
            let Some(s1) = t[i1].pop() else {
                anyhow::bail!("Move: empty stack")
            };
            if let Some(s2) = t[i2].last() {
                if s2 < &s1 {
                    anyhow::bail!("Move: taille de la pile de destination trop petite");
                }
            }
            t[i2].push(s1);
            // Print current state
            for h in (0..5).rev() {
                if let Some(tile) = t[0].get(h) {
                    r.print(tile)?;
                } else {
                    r.print(" ")?;
                }
                r.print("|")?;
                if let Some(tile) = t[1].get(h) {
                    r.print(tile)?;
                } else {
                    r.print(" ")?;
                }
                r.print("|")?;
                if let Some(tile) = t[2].get(h) {
                    r.print(tile)?;
                } else {
                    r.print(" ")?;
                }
                r.print("\n")?;
            }
            Ok(0)
        };
        interpreter.add_builtin("move".to_string(), Box::new(mv));
        interpreter.add_builtin("get".to_string(), Box::new(get));
        interpreter.add_builtin("set".to_string(), Box::new(set));
        interpreter.add_builtin("swap".to_string(), Box::new(swap));
        interpreter.add_builtin("verifier".to_string(), Box::new(verifier));
        let res = interpreter.run_timeout(timeout);
        self.program_output = res.stdout;
        self.program_errors = res.error.unwrap_or_else(String::new);
    }
}

impl eframe::App for TemplateApp {
    // Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            ui.add_space(5.);
            ui.horizontal(|ui| {
                ui.heading("SECS Web IDE");
                let btn = Button::new(RichText::new("Run ▶").color(Color32::WHITE).heading())
                    .fill(Color32::DARK_GREEN);
                if ui.add(btn).clicked() {
                    self.run_user_code();
                }
                for (w, var) in [
                    ("Editor Window", &mut self.show_editor),
                    ("Program Output", &mut self.show_output),
                    ("Program Errors", &mut self.show_errors),
                    ("Runner options", &mut self.show_runoptions),
                    ("SECS Grammar", &mut self.show_grammar),
                    ("Help", &mut self.show_help),
                    ("Partie I", &mut self.show_part1),
                    ("Partie II", &mut self.show_part2),
                ] {
                    if ui.selectable_label(*var, w).clicked() {
                        *var = !*var;
                    }
                }
            });
            ui.add_space(5.);
        });

        // Draw editor window
        if self.show_editor {
            egui::Window::new("Editor Window").show(ctx, |ui| {
                ui.add_sized(
                    ui.available_size(),
                    egui::widgets::TextEdit::multiline(&mut self.codeinput)
                        .code_editor()
                        .font(FontId::new(20., egui::FontFamily::Monospace)),
                );
            });
        }

        if self.show_runoptions {
            egui::Window::new("Runner options").show(ctx, |ui| {
                ui.vertical(|ui| {
                    ui.add(egui::widgets::Checkbox::new(
                        &mut self.timeout_enabled,
                        "Enable timeout",
                    ));
                    if self.timeout_enabled {
                        ui.add(
                            egui::widgets::Slider::new(&mut self.timeout_value, 1..=2000)
                                .integer()
                                .text("Timeout in ms"),
                        );
                    }
                })
            });
        }
        if self.show_output {
            egui::Window::new("Program Output").show(ctx, |ui| {
                egui::ScrollArea::new([false, true]).show(ui, |ui| {
                    egui::widgets::TextEdit::multiline(&mut self.program_output)
                        .code_editor()
                        .interactive(false)
                        .font(FontId::new(20., egui::FontFamily::Monospace))
                        .show(ui)
                });
            });
        }
        if self.show_errors {
            egui::Window::new("Program Errors").show(ctx, |ui| {
                ui.add_sized(
                    ui.available_size(),
                    egui::widgets::TextEdit::multiline(&mut self.program_errors)
                        .code_editor()
                        .interactive(false)
                        .font(FontId::new(20., egui::FontFamily::Monospace)),
                );
            });
        }
        if self.show_grammar {
            let grammar = include_str!("../../parser/src/grammar.pest");
            egui::Window::new("Grammar").show(ctx, |ui| {
                ui.label("Définition formelle de la grammaire du SECS:");
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show(ui, |ui| ui.label(RichText::new(grammar).monospace()))
            });
        }
        if self.show_help {
            egui::Window::new("Help").show(ctx, |ui| {
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show(ui, |ui| ui.label(RichText::new(HELP_TEXT).monospace()))
            });
        }
        if self.show_part1 {
            egui::Window::new("Partie I").show(ctx, |ui| {
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show(ui, |ui| {
                        ui.label(RichText::new(HELP_PART1).size(18.).monospace())
                    })
            });
        }
        if self.show_part2 {
            egui::Window::new("Partie II").show(ctx, |ui| {
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show(ui, |ui| {
                        ui.label(RichText::new(HELP_PART2).size(18.).monospace())
                    })
            });
        }
    }
}

fn random_vec(size: usize) -> Vec<usize> {
    (0..size).map(|_| thread_rng().gen_range(0..100)).collect()
}
