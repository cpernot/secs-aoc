use inkwell::OptimizationLevel;
#[derive(Clone, Debug)]
pub struct CompilerOptions {
    pub opt: OptimizationLevel,
    pub stack_size: usize,
}
impl Default for CompilerOptions {
    fn default() -> Self {
        Self {
            opt: OptimizationLevel::Aggressive,
            stack_size: 8192,
        }
    }
}
