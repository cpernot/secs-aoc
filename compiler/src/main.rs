use std::path::PathBuf;

use clap::Parser;
use compiler::Compiler;
use inkwell::context::Context;
use linker::Linker;

mod compiler;
mod linker;
mod options;

#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long, help = "The output object file")]
    output: PathBuf,
    #[arg(short, help = "Do not link an executable")]
    compile_only: bool,
    #[arg(short, long, help = "Link external library")]
    lib: Vec<PathBuf>,
    source: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    let code = std::fs::read_to_string(&args.source)?;
    let ast = secs_parser::parse(&code)?;
    let context = Context::create();
    let compiler = Compiler::create(&context)?;
    compiler.compile(ast)?;
    if args.compile_only {
        compiler.output(&args.output)?;
    } else {
        let tmpfile = tempfile::Builder::new().suffix(".à").tempfile()?;
        let linker = Linker::new();
        compiler.output(tmpfile.path())?;
        linker.link([tmpfile.path()], &args.lib, &args.output)?;
        drop(tmpfile)
    }
    Ok(())
}
