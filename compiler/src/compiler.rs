use std::path::Path;

use anyhow::anyhow;
use inkwell::{
    basic_block::BasicBlock,
    builder::Builder,
    context::Context,
    execution_engine::ExecutionEngine,
    module::Module,
    targets::{CodeModel, FileType, Target, TargetMachine},
    types::{FunctionType, IntType, PointerType},
    values::{BasicMetadataValueEnum, BasicValue, FunctionValue, IntValue, PointerValue},
    AddressSpace, IntPredicate,
};

use secs_parser::ast::{
    BinOpKind, Condition, ConditionKind, Literal, Modifier, Operation, Program, TopLevelDeclaration,
};

use crate::options::CompilerOptions;

pub struct Compiler<'ctx> {
    ctx: &'ctx Context,
    options: CompilerOptions,
    builder: Builder<'ctx>,
    module: Module<'ctx>,
    _execengine: ExecutionEngine<'ctx>,
    stackptr: PointerValue<'ctx>,
    inttype: IntType<'ctx>,
    generic_ptr_type: PointerType<'ctx>,
    userfntype: FunctionType<'ctx>,
}
pub struct FnBuilder<'c, 'ctx> {
    compiler: &'c Compiler<'ctx>,
    func: FunctionValue<'ctx>,
    curr_block: BasicBlock<'ctx>,
}
impl<'c, 'ctx> FnBuilder<'c, 'ctx> {
    fn new(compiler: &'c Compiler<'ctx>, fnname: &str) -> anyhow::Result<Self> {
        let func = compiler.locate_function(fnname)?;
        let curr_block = compiler.ctx.append_basic_block(func, "entry");
        Ok(Self {
            compiler,
            func,
            curr_block,
        })
    }
    fn set_block(&mut self, b: BasicBlock<'ctx>) {
        self.compiler.builder.position_at_end(b);
        self.curr_block = b;
    }
    fn add_block(&self, name: &str) -> BasicBlock<'ctx> {
        let b = self.compiler.ctx.append_basic_block(self.func, name);
        self.compiler.builder.position_at_end(self.curr_block);
        b
    }
    fn build_get_stackaddr(&self) -> anyhow::Result<IntValue<'ctx>> {
        let sptr = self
            .compiler
            .builder
            .build_load(
                self.compiler.stackptr.get_type(),
                self.compiler.stackptr,
                "",
            )?
            .into_pointer_value();
        let sptr = self
            .compiler
            .builder
            .build_ptr_to_int(sptr, self.compiler.inttype, "ptrint")?;
        Ok(sptr)
    }
    fn build_stack_popn(&self, n: usize) -> anyhow::Result<()> {
        // On augmente le stackptr de n éléments
        let sptr = self.build_get_stackaddr()?;
        let elemsize = self.compiler.inttype.size_of();
        let diff = elemsize.const_mul(elemsize.get_type().const_int(n as u64, false));
        let sptr = self
            .compiler
            .builder
            .build_int_add(sptr, diff, "stackdiff")?;
        self.compiler
            .builder
            .build_store(self.compiler.stackptr, sptr)?;
        Ok(())
    }
    fn build_stack_pop(&self) -> anyhow::Result<()> {
        // On augmente le stackptr
        let sptr = self.build_get_stackaddr()?;
        let diff = self.compiler.inttype.size_of();
        let sptr = self
            .compiler
            .builder
            .build_int_add(sptr, diff, "stackdiff")?;
        self.compiler
            .builder
            .build_store(self.compiler.stackptr, sptr)?;
        Ok(())
    }
    fn build_stack_push<T: BasicValue<'ctx>>(&self, value: T) -> anyhow::Result<()> {
        // On diminue le stackptr
        let sptr = self.build_get_stackaddr()?;
        let diff = self.compiler.inttype.size_of();
        let sptr = self
            .compiler
            .builder
            .build_int_sub(sptr, diff, "stackdiff")?;
        self.compiler
            .builder
            .build_store(self.compiler.stackptr, sptr)?;
        // On stocke la valeur dans le stackptr
        self.compiler.builder.build_store(
            self.compiler
                .builder
                .build_int_to_ptr(sptr, self.compiler.stackptr.get_type(), "")?,
            value,
        )?;
        Ok(())
    }
    fn build_stack_get(&self, offset: Option<usize>) -> anyhow::Result<IntValue<'ctx>> {
        let sptr = self.build_get_stackaddr()?;
        let elemaddr = match offset {
            Some(n) => {
                let elemsize = self.compiler.generic_ptr_type.size_of();
                let diff = elemsize.const_mul(elemsize.get_type().const_int(n as u64, false));
                self.compiler
                    .builder
                    .build_int_add(sptr, diff, "elemaddr")?
            }
            None => sptr,
        };
        let elemptr = self.compiler.builder.build_int_to_ptr(
            elemaddr,
            self.compiler.stackptr.get_type(),
            "elemptr",
        )?;
        let elem = self
            .compiler
            .builder
            .build_load(self.compiler.inttype, elemptr, "stackderef")?
            .into_int_value();
        Ok(elem)
    }
    fn build_stack_set<V: BasicValue<'ctx>>(
        &self,
        offset: Option<usize>,
        val: V,
    ) -> anyhow::Result<()> {
        let sptr = self.build_get_stackaddr()?;
        let elemaddr = match offset {
            Some(n) => {
                let elemsize = self.compiler.generic_ptr_type.size_of();
                let diff = elemsize.const_mul(elemsize.get_type().const_int(n as u64, false));
                self.compiler
                    .builder
                    .build_int_add(sptr, diff, "elemaddr")?
            }
            None => sptr,
        };
        let elemptr = self.compiler.builder.build_int_to_ptr(
            elemaddr,
            self.compiler.stackptr.get_type(),
            "elemptr",
        )?;
        self.compiler.builder.build_store(elemptr, val)?;
        Ok(())
    }
    fn compile_condition(&self, cond: &Condition) -> anyhow::Result<IntValue<'ctx>> {
        let predicate = match cond.cond {
            ConditionKind::Lt => IntPredicate::ULT,
            ConditionKind::Le => IntPredicate::ULE,
            ConditionKind::Eq => IntPredicate::EQ,
            ConditionKind::Ne => IntPredicate::NE,
            ConditionKind::Gt => IntPredicate::UGT,
            ConditionKind::Ge => IntPredicate::UGE,
        };
        let v1 = self.build_stack_get(None)?;
        let v2 = if let Some(cst) = cond.member {
            self.compiler.inttype.const_int(cst as u64, true)
        } else {
            self.build_stack_get(Some(1))?
        };
        let cmp = self
            .compiler
            .builder
            .build_int_compare(predicate, v1, v2, "")?;
        match cond.modifier {
            Modifier::Preserve => {}
            Modifier::Destroy => {
                self.build_stack_popn(if cond.member.is_some() { 1 } else { 2 })?;
            }
        }
        Ok(cmp)
    }
    fn compile_op(&mut self, op: &Operation) -> anyhow::Result<()> {
        match op {
            Operation::Push(lit) => {
                match lit {
                    Literal::Integer(n) => {
                        let val = self.compiler.inttype.const_int(*n as u64, true);
                        self.build_stack_push(val)?;
                    }
                    Literal::Str(s) => {
                        let val = self
                            .compiler
                            .builder
                            .build_global_string_ptr(s.as_str(), "")?;
                        self.build_stack_push(val)?;
                    }
                };
            }
            Operation::Pop => {
                self.build_stack_pop()?;
            }
            Operation::FnCall(fnname, modifier, nbargs, discard) => {
                let args: Vec<BasicMetadataValueEnum> = (0..*nbargs)
                    .map(|n| {
                        self.build_stack_get(Some(nbargs - 1 - n))
                            .map(BasicMetadataValueEnum::from)
                    })
                    .collect::<Result<Vec<_>, _>>()?;
                let f = self.compiler.locate_function(mangle_userfn(fnname))?;
                match *modifier {
                    Modifier::Destroy => {
                        self.build_stack_popn(*nbargs)?;
                    }
                    Modifier::Preserve => {}
                }
                let result = self.compiler.builder.build_call(f, &args, "fncall")?;
                if !discard {
                    self.build_stack_push(
                        result
                            .try_as_basic_value()
                            .left()
                            .expect("Function should return a value"),
                    )?;
                }
            }
            Operation::Increment => {
                let val = self.build_stack_get(None)?;
                let vali = self.compiler.builder.build_int_add(
                    val,
                    self.compiler.inttype.const_int(1, false),
                    "",
                )?;
                self.build_stack_set(None, vali)?;
            }
            Operation::IfElse(condition, thencode, elsecode) => {
                let condition = self.compile_condition(condition)?;
                let block_then = self.add_block("then");
                let block_else = self.add_block("else");
                let block_end = self.add_block("end");
                self.compiler
                    .builder
                    .build_conditional_branch(condition, block_then, block_else)?;

                // Build "then" branch
                self.set_block(block_then);
                self.compile_ops(thencode)?;
                self.compiler
                    .builder
                    .build_unconditional_branch(block_end)?;

                // Build "else" branch
                self.set_block(block_else);
                self.compile_ops(elsecode)?;
                self.compiler
                    .builder
                    .build_unconditional_branch(block_end)?;

                self.set_block(block_end);
            }
            Operation::Loop(condition, inner) => {
                let old_block = self.curr_block;
                let conditionblock = self.add_block("loopcond");
                let loopbody = self.add_block("loopbody");
                let nextblock = self.add_block("loopend");
                self.set_block(old_block);
                self.compiler
                    .builder
                    .build_unconditional_branch(conditionblock)?;

                // Build condition
                self.set_block(conditionblock);
                let cond = self.compile_condition(condition)?;
                self.compiler
                    .builder
                    .build_conditional_branch(cond, loopbody, nextblock)?;

                // Build body
                self.set_block(loopbody);
                self.compile_ops(inner)?;
                // After body, loop back to condition
                self.compiler
                    .builder
                    .build_unconditional_branch(conditionblock)?;
                self.set_block(nextblock);
            }
            Operation::BinOp(opkind, modifier) => {
                let v1 = self.build_stack_get(None)?;
                let v2 = self.build_stack_get(Some(1))?;
                let res = match opkind {
                    BinOpKind::Add => self.compiler.builder.build_int_add(v1, v2, "")?,
                    BinOpKind::Sub => self.compiler.builder.build_int_sub(v1, v2, "")?,
                    BinOpKind::Mul => self.compiler.builder.build_int_mul(v1, v2, "")?,
                    BinOpKind::Div => self.compiler.builder.build_int_unsigned_div(v1, v2, "")?,
                    BinOpKind::Rem => self.compiler.builder.build_int_unsigned_rem(v1, v2, "")?,
                };
                match modifier {
                    Modifier::Preserve => {
                        self.build_stack_push(res)?;
                    }
                    Modifier::Destroy => {
                        self.build_stack_pop()?;
                        self.build_stack_set(None, res)?;
                    }
                }
            }
            Operation::Swap => {
                let v1 = self.build_stack_get(None)?;
                let v2 = self.build_stack_get(Some(1))?;
                self.build_stack_set(None, v2)?;
                self.build_stack_set(Some(1), v1)?;
            }
            Operation::Clone => {
                let v = self.build_stack_get(None)?;
                self.build_stack_push(v)?;
            }
            Operation::Deref(modifier) => {
                let intptr = self.build_stack_get(None)?;
                let ptrval = self.compiler.builder.build_int_to_ptr(
                    intptr,
                    self.compiler.generic_ptr_type,
                    "",
                )?;
                let innerval =
                    self.compiler
                        .builder
                        .build_load(self.compiler.inttype, ptrval, "")?;
                match modifier {
                    Modifier::Preserve => {
                        self.build_stack_push(innerval)?;
                    }
                    Modifier::Destroy => {
                        self.build_stack_set(None, innerval)?;
                    }
                }
            }
            Operation::Load(modifier, i) => {
                let (ptr, index) = match i {
                    Some(i) => (
                        self.build_stack_get(None)?,
                        self.compiler.inttype.const_int(*i as u64, false),
                    ),
                    None => (self.build_stack_get(Some(1))?, self.build_stack_get(None)?),
                };
                let ptrint = self.compiler.builder.build_int_add(
                    ptr,
                    self.compiler.builder.build_int_mul(
                        index,
                        self.compiler.inttype.size_of(),
                        "",
                    )?,
                    "",
                )?;
                let elemptr = self.compiler.builder.build_int_to_ptr(
                    ptrint,
                    self.compiler.generic_ptr_type,
                    "",
                )?;
                let elem = self
                    .compiler
                    .builder
                    .build_load(self.compiler.inttype, elemptr, "")?;
                match modifier {
                    Modifier::Preserve => {
                        self.build_stack_push(elem)?;
                    }
                    Modifier::Destroy => {
                        match i {
                            None => {
                                self.build_stack_pop()?;
                            }
                            Some(_) => {}
                        };
                        self.build_stack_set(None, elem)?;
                    }
                }
            }
            Operation::Store(modifier, i) => {
                let (ptr, index, val) = match i {
                    Some(n) => (
                        self.build_stack_get(Some(1))?,
                        self.compiler.inttype.const_int(*n as u64, false),
                        self.build_stack_get(None)?,
                    ),
                    None => (
                        self.build_stack_get(Some(2))?,
                        self.build_stack_get(Some(1))?,
                        self.build_stack_get(None)?,
                    ),
                };
                let ptrint = self.compiler.builder.build_int_add(
                    ptr,
                    self.compiler.builder.build_int_mul(
                        index,
                        self.compiler.inttype.size_of(),
                        "",
                    )?,
                    "",
                )?;
                let elemptr = self.compiler.builder.build_int_to_ptr(
                    ptrint,
                    self.compiler.generic_ptr_type,
                    "",
                )?;
                self.compiler.builder.build_store(elemptr, val)?;
                match modifier {
                    Modifier::Destroy => {
                        self.build_stack_popn(3)?;
                    }
                    Modifier::Preserve => {}
                }
            }
            Operation::Raise(n) => {
                let elemval = self.build_stack_get(Some(*n))?;
                self.build_stack_push(elemval)?;
            }
            Operation::LoadGlobal(name) => {
                let elemptr = self.compiler.locate_global(name)?;
                let val = self.compiler.builder.build_load(
                    self.compiler.inttype,
                    elemptr,
                    "global_val",
                )?;
                self.build_stack_push(val)?;
            }
            Operation::StoreGlobal(name, modifier) => {
                let val = self.build_stack_get(None)?;
                let elemptr = self.compiler.locate_global(name)?;
                self.compiler.builder.build_store(elemptr, val)?;
                match modifier {
                    Modifier::Preserve => {}
                    Modifier::Destroy => self.build_stack_pop()?,
                }
            }
        }
        Ok(())
    }
    fn compile_ops(&mut self, ops: &[Operation]) -> anyhow::Result<()> {
        for op in ops {
            self.compile_op(op)?;
        }
        Ok(())
    }
    pub fn compile(&mut self, ops: &[Operation]) -> anyhow::Result<()> {
        self.compiler.builder.position_at_end(self.curr_block);
        self.compile_ops(ops).unwrap();
        self.compiler.builder.build_return(None)?;
        Ok(())
    }
}
impl<'ctx> Compiler<'ctx> {
    pub fn create(ctx: &'ctx Context) -> anyhow::Result<Self> {
        Self::create_with_options(ctx, CompilerOptions::default())
    }
    pub fn create_with_options(
        ctx: &'ctx Context,
        options: CompilerOptions,
    ) -> anyhow::Result<Self> {
        let builder = ctx.create_builder();
        let module = ctx.create_module("stdlib");
        let target = TargetMachine::get_default_triple();
        module.set_triple(&target);
        let execengine = module.create_execution_engine().unwrap();
        let target_data = execengine.get_target_data();
        let stacktype = ctx.i64_type().ptr_type(AddressSpace::default());
        let stackptr = module
            .add_global(stacktype, None, "stack")
            .as_pointer_value();
        let voidtype = ctx.void_type();
        let inttype = ctx.ptr_sized_int_type(target_data, Default::default());
        let _self = Self {
            ctx,
            options,
            builder,
            module,
            _execengine: execengine,
            stackptr,
            inttype,
            generic_ptr_type: inttype.ptr_type(Default::default()),
            userfntype: voidtype.fn_type(&[], false),
        };
        Ok(_self)
    }
    // Creates the code of the main function
    // The main function allocates the language stack and
    fn init_main(&self) -> anyhow::Result<()> {
        let _main = self
            .module
            .add_function("main", self.ctx.i32_type().fn_type(&[], false), None);
        let malloc = self.module.add_function(
            "malloc",
            self.generic_ptr_type.fn_type(&[self.inttype.into()], false),
            None,
        );
        let block = self.ctx.append_basic_block(_main, "mainblock");
        self.builder.position_at_end(block);
        let alloc_size = self
            .inttype
            .const_int(self.options.stack_size as u64, false)
            .const_mul(self.inttype.size_of());
        let alloc_data = self
            .builder
            .build_call(malloc, &[alloc_size.into()], "stackmalloc")?
            .try_as_basic_value()
            .left()
            .expect("malloc should return a value")
            .into_pointer_value();
        // ptr = alloc_data + (alloc_size - sizeof element)
        let sptr = self.builder.build_int_add(
            self.builder
                .build_ptr_to_int(alloc_data, self.inttype, "")?,
            self.builder
                .build_int_sub(alloc_size, self.inttype.size_of(), "")?,
            "",
        )?;
        self.builder.build_store(self.stackptr, sptr)?;
        let usermain = self.locate_function("_main")?;
        self.builder.build_call(usermain, &[], "maincall")?;
        // free(sptr); sptr = NULL
        self.builder.build_free(alloc_data)?;
        self.builder
            .build_store(self.stackptr, self.generic_ptr_type.const_null())?;
        self.builder
            .build_return(Some(&self.ctx.i32_type().const_int(0, false)))?;
        // Initialise global variable stack to NULL
        self.module
            .get_global("stack")
            .unwrap()
            .set_initializer(&self.stackptr.get_type().const_null());
        Ok(())
    }
    pub fn locate_function(&self, name: &str) -> anyhow::Result<FunctionValue<'ctx>> {
        self.module
            .get_function(name)
            .ok_or_else(|| anyhow!("Unknown function: {}", name))
    }
    pub fn locate_global(&self, name: &str) -> anyhow::Result<PointerValue<'ctx>> {
        self.module
            .get_global(name)
            .map(|g| g.as_pointer_value())
            .ok_or_else(|| anyhow!("Unknown global `{}`", name))
    }
    pub fn compile(&self, p: Program) -> anyhow::Result<()> {
        // First declare all functions defined in this module
        for f in &p.items {
            match f {
                TopLevelDeclaration::FunctionDef(fnname, _ops) => {
                    self.module
                        .add_function(mangle_userfn(fnname), self.userfntype, None);
                }
                TopLevelDeclaration::FunctionDecl(fnname, nbargs, var_args) => {
                    let argstype = (0..*nbargs)
                        .map(|_| self.inttype.into())
                        .collect::<Vec<_>>();
                    self.module.add_function(
                        mangle_userfn(fnname),
                        self.inttype.fn_type(&argstype, *var_args),
                        None,
                    );
                }
                TopLevelDeclaration::GlobalDecl(name) => {
                    self.module
                        .add_global(self.inttype, None, name)
                        .set_initializer(&self.inttype.const_int(0, false));
                }
            }
        }
        for f in p.items {
            match f {
                TopLevelDeclaration::FunctionDef(fnname, ops) => {
                    let is_main = fnname == "main";
                    let name = mangle_userfn(&fnname);
                    let mut fnbuilder: FnBuilder = FnBuilder::new(self, name)?;
                    if is_main {
                        // This is the main file: define main
                        self.init_main()?;
                    }
                    fnbuilder.compile(&ops)?;
                }
                TopLevelDeclaration::FunctionDecl(_, _, _) => {}
                TopLevelDeclaration::GlobalDecl(_) => {}
            }
        }
        if let Err(err) = self.module.verify() {
            self.module.print_to_stderr();
            return Err(anyhow!("{}", err));
        }
        Ok(())
    }
    pub fn output(&self, path: &Path) -> anyhow::Result<()> {
        let target = self.module.get_triple();
        let target_machine = Target::from_triple(&target)
            .unwrap()
            .create_target_machine(
                &target,
                "generic",
                "",
                self.options.opt,
                inkwell::targets::RelocMode::PIC,
                CodeModel::Default,
            )
            .unwrap();
        if let Err(e) = target_machine.write_to_file(&self.module, FileType::Object, path) {
            return Err(anyhow!(e.to_string()));
        }
        Ok(())
    }
    /*
    pub unsafe fn run_main(&self) -> anyhow::Result<()> {
        unsafe {
            self._execengine
                .run_function_as_main(self.locate_function("main")?, &[]);
        }
        Ok(())
    }
    */
}

fn mangle_userfn(s: &str) -> &str {
    match s {
        "main" => "_main",
        _ => s,
    }
}
