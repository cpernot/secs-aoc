use std::{ffi::OsStr, path::Path, process::Command};

use anyhow::anyhow;

pub struct Linker {}

impl Linker {
    pub fn new() -> Self {
        Self {}
    }
    pub fn link<I, S, J, O>(&self, files: I, libs: J, output: &Path) -> anyhow::Result<()>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
        J: IntoIterator<Item = O>,
        O: AsRef<OsStr>,
    {
        let mut command = Command::new("ld");
        command
            .arg("-pie")
            .arg("-dynamic-linker")
            .arg("/lib64/ld-linux-x86-64.so.2")
            .arg("/lib/crt1.o")
            .arg("-lc");
        command.args(files).arg("-o").arg(output);
        for l in libs {
            command.arg("-l").arg(l);
        }
        if command.spawn()?.wait()?.success() {
            Ok(())
        } else {
            Err(anyhow!("Call to ld returned an error"))
        }
    }
}
