use std::{
    alloc::Layout,
    collections::{BTreeMap, HashMap},
    fmt::Display,
    fmt::Write,
};

use anyhow::anyhow;
use secs_parser::ast::{
    BinOpKind, Condition, ConditionKind, Literal, Modifier, Operation, TopLevelDeclaration,
};

pub type BuiltinFn = Box<dyn Fn(&mut Runner, Vec<usize>) -> anyhow::Result<usize>>;
enum FnEnum {
    // Unlike in compiled code, the native function can access the stack to push their result, if any.
    Native(BuiltinFn),
    User(Vec<Operation>),
}

pub struct Interpreter {
    functions: HashMap<String, FnEnum>,
    globals: BTreeMap<String, usize>,
    #[cfg(feature = "timeout")]
    stop: Option<instant::Instant>,
}
pub struct Runner<'i> {
    stdout: String,
    stack: Vec<usize>,
    globals: BTreeMap<String, usize>,
    interpreter: &'i Interpreter,
    allocmgr: AllocManager,
}
pub struct ExecutionResult {
    pub stdout: String,
    pub error: Option<String>,
}
#[derive(Default)]
pub struct AllocManager {
    pointers: BTreeMap<*mut usize, usize>,
}
impl AllocManager {
    pub fn alloc(&mut self, size: usize) -> *mut usize {
        let ptr = unsafe {
            std::alloc::alloc(
                Layout::from_size_align(size, std::mem::size_of::<usize>())
                    .expect("Can't create memory layout"),
            )
            .cast()
        };
        self.pointers.insert(ptr, size);
        ptr
    }
    pub fn has(&self, ptr: *mut usize) -> bool {
        self.pointers.contains_key(&ptr)
    }
    pub fn free(&mut self, ptr: *mut usize) -> anyhow::Result<()> {
        let Some(size) = self.pointers.remove(&ptr) else {
            return Err(anyhow!("free() called on invalid pointer"));
        };
        unsafe {
            std::alloc::dealloc(
                ptr.cast(),
                Layout::from_size_align(size, std::mem::size_of::<usize>())
                    .expect("Can't create memory layout"),
            );
        }
        Ok(())
    }
}
impl Drop for AllocManager {
    fn drop(&mut self) {
        for (p, size) in &self.pointers {
            unsafe {
                std::alloc::dealloc(
                    p.cast(),
                    Layout::from_size_align(*size, std::mem::size_of::<usize>())
                        .expect("Can't create memory layout"),
                );
            }
        }
    }
}

impl Interpreter {
    pub fn new(code: &str) -> anyhow::Result<Self> {
        let prog = secs_parser::parse(code)?;
        let mut functions = HashMap::new();
        let mut globals = BTreeMap::new();
        for i in prog.items {
            match i {
                TopLevelDeclaration::FunctionDef(fnname, ops) => {
                    functions.insert(fnname, FnEnum::User(ops));
                }
                TopLevelDeclaration::FunctionDecl(_, _, _) => {}
                TopLevelDeclaration::GlobalDecl(name) => {
                    globals.insert(name, 0);
                }
            }
        }
        let mut i = Self {
            functions,
            globals,
            #[cfg(feature = "timeout")]
            stop: None,
        };
        i.insert_builtins();
        Ok(i)
    }
    fn insert_builtins(&mut self) {
        fn print_string(interp: &mut Runner, args: Vec<usize>) -> anyhow::Result<usize> {
            if args.len() != 1 {
                return Err(anyhow!("print_string takes 1 argument"));
            }
            unsafe {
                let s = args[0] as *const String;
                interp.stdout.push_str((*s).as_str());
            }
            Ok(0)
        }
        fn print_int(interp: &mut Runner, args: Vec<usize>) -> anyhow::Result<usize> {
            if args.len() != 1 {
                return Err(anyhow!("print_int takes 1 argument"));
            }
            interp.stdout.push_str(&args[0].to_string());
            Ok(0)
        }
        fn malloc(runner: &mut Runner, args: Vec<usize>) -> anyhow::Result<usize> {
            if args.len() == 1 {
                Ok(runner.allocmgr.alloc(args[0]) as usize)
            } else {
                Err(anyhow!("malloc takes 1 argument"))
            }
        }
        fn free(runner: &mut Runner, args: Vec<usize>) -> anyhow::Result<usize> {
            runner.allocmgr.free(args[0] as *mut usize)?;
            Ok(0)
        }
        fn print_stack(runner: &mut Runner, args: Vec<usize>) -> anyhow::Result<usize> {
            if !args.is_empty() {
                return Err(anyhow!("print_stack takes 0 argument"));
            }
            runner.stdout.push_str(&format!("{:?}", runner.stack));
            Ok(0)
        }
        self.functions.insert(
            "print_string".to_owned(),
            FnEnum::Native(Box::new(print_string)),
        );
        self.functions
            .insert("print_int".to_owned(), FnEnum::Native(Box::new(print_int)));
        self.functions.insert(
            "print_stack".to_owned(),
            FnEnum::Native(Box::new(print_stack)),
        );
        self.functions
            .insert("malloc".to_owned(), FnEnum::Native(Box::new(malloc)));
        self.functions
            .insert("free".to_owned(), FnEnum::Native(Box::new(free)));
    }
    pub fn add_builtin(&mut self, name: String, builtin: BuiltinFn) {
        self.functions.insert(name, FnEnum::Native(builtin));
    }
    pub fn add_global(&mut self, name: String, value: usize) {
        self.globals.insert(name, value);
    }
    fn runner(&mut self) -> Runner {
        // Initialise globals to 0
        let globals = self.globals.clone();
        Runner {
            stdout: String::new(),
            stack: Vec::new(),
            globals,
            interpreter: self,
            allocmgr: Default::default(),
        }
    }
    #[cfg(feature = "timeout")]
    pub fn clear_timeout(&mut self) {
        self.stop = None;
    }
    #[cfg(feature = "timeout")]
    pub fn run_timeout(&mut self, timeout_ms: u64) -> ExecutionResult {
        self.stop = Some(instant::Instant::now() + instant::Duration::from_millis(timeout_ms));
        self.run()
    }
    pub fn run(&mut self) -> ExecutionResult {
        let mut runner = self.runner();
        let res = runner.run();
        ExecutionResult {
            stdout: runner.finish(),
            error: res.err().map(|err| err.to_string()),
        }
    }
}
impl<'i> Runner<'i> {
    pub fn run(&mut self) -> anyhow::Result<()> {
        self.run_function("main", [].to_vec())?;
        Ok(())
    }
    pub fn finish(self) -> String {
        self.stdout
    }
    pub fn print<D: Display>(&mut self, s: D) -> anyhow::Result<()> {
        write!(self.stdout, "{}", s)?;
        Ok(())
    }
    pub fn pop(&mut self) -> anyhow::Result<usize> {
        self.stack
            .pop()
            .ok_or_else(|| anyhow!("Cannot pop an empty stack"))
    }
    pub fn push(&mut self, val: usize) {
        self.stack.push(val);
    }
    pub fn top(&mut self) -> anyhow::Result<usize> {
        self.stack
            .last()
            .copied()
            .ok_or_else(|| anyhow!("Cannot access empty stack"))
    }
    pub fn top_offset(&mut self, offset: usize) -> anyhow::Result<usize> {
        let l = self.stack.len();
        l.checked_sub(offset + 1)
            .and_then(|i| self.stack.get(i).copied())
            .ok_or_else(|| anyhow!("Stack too small for {} offset", offset))
    }
    pub(crate) fn run_cond(&mut self, cond: Condition) -> anyhow::Result<bool> {
        let v1 = self.top()? as isize;
        let v2 = if let Some(m) = cond.member {
            m
        } else {
            self.top_offset(1)? as isize
        };
        let res = match cond.cond {
            ConditionKind::Lt => v1 < v2,
            ConditionKind::Le => v1 <= v2,
            ConditionKind::Eq => v1 == v2,
            ConditionKind::Ne => v1 != v2,
            ConditionKind::Gt => v1 > v2,
            ConditionKind::Ge => v1 >= v2,
        };
        match cond.modifier {
            Modifier::Preserve => {}
            Modifier::Destroy => {
                self.pop()?;
                if cond.member.is_none() {
                    self.pop()?;
                }
            }
        }
        Ok(res)
    }
    fn run_instructions(&mut self, ins: &[Operation]) -> anyhow::Result<()> {
        for op in ins {
            match op {
                Operation::BinOp(kind, modifier) => {
                    let n1 = self.top()? as isize;
                    let n2 = self.top_offset(1)? as isize;
                    let res = match kind {
                        BinOpKind::Add => n1 + n2,
                        BinOpKind::Sub => n1 - n2,
                        BinOpKind::Mul => n1 * n2,
                        BinOpKind::Div => {
                            n1.checked_div(n2).ok_or_else(|| anyhow!("Zero Division"))?
                        }
                        BinOpKind::Rem => {
                            n1.checked_rem(n2).ok_or_else(|| anyhow!("Zero Division"))?
                        }
                    };
                    match modifier {
                        Modifier::Preserve => {}
                        Modifier::Destroy => {
                            self.pop()?;
                            self.pop()?;
                        }
                    }
                    self.stack.push(res as usize);
                }
                Operation::Clone => {
                    let val = self.top()?;
                    self.stack.push(val);
                }
                Operation::Deref(modifier) => {
                    let p = match modifier {
                        Modifier::Preserve => self.top()?,
                        Modifier::Destroy => self.pop()?,
                    };
                    let ptr: *mut usize = p as *mut usize;
                    let val = unsafe { *ptr };
                    self.stack.push(val);
                }
                Operation::FnCall(name, modifier, nbargs, discard) => {
                    let args: Vec<_> = match modifier {
                        Modifier::Destroy => self
                            .stack
                            .drain(
                                (self.stack.len().checked_sub(*nbargs).ok_or_else(|| {
                                    anyhow!("Pas assez d'arguments dans la pile")
                                })?)..,
                            )
                            .collect(),
                        Modifier::Preserve => self
                            .stack
                            .iter()
                            .rev()
                            .take(*nbargs)
                            .rev()
                            .copied()
                            .collect(),
                    };
                    let result = self.run_function(name, args)?;
                    if !discard {
                        self.stack.push(result);
                    }
                }
                Operation::IfElse(cond, thenops, elseops) => {
                    if self.run_cond(*cond)? {
                        self.run_instructions(thenops)?;
                    } else {
                        self.run_instructions(elseops)?;
                    }
                }
                Operation::Increment => {
                    let v = self
                        .stack
                        .last_mut()
                        .ok_or_else(|| anyhow!("Stack is empty"))?;
                    *v += 1;
                }
                Operation::Load(modifier, optindex) => {
                    let (ptrv, index) = match optindex {
                        Some(i) => (self.top()?, *i),
                        None => (self.top_offset(1)?, self.top()?),
                    };
                    let ptr = ptrv as *mut usize;
                    let val = unsafe { *ptr.add(index) };
                    match modifier {
                        Modifier::Preserve => {}
                        Modifier::Destroy => {
                            self.pop()?;
                            if optindex.is_none() {
                                self.pop()?;
                            }
                        }
                    }
                    self.stack.push(val);
                }
                Operation::Loop(cond, ops) => {
                    while self.run_cond(*cond)? {
                        self.run_instructions(ops)?;
                        #[cfg(feature = "timeout")]
                        if let Some(stop) = self.interpreter.stop {
                            if instant::Instant::now() > stop {
                                return Err(anyhow!("Timeout in execution"));
                            }
                        }
                    }
                }
                Operation::Pop => {
                    self.pop()?;
                }
                Operation::Push(lit) => match lit {
                    Literal::Integer(i) => self.stack.push(*i as usize),
                    Literal::Str(s) => self.stack.push(s as *const String as usize),
                },
                Operation::Raise(n) => {
                    let val = self
                        .stack
                        .get(self.stack.len() - 1 - n)
                        .copied()
                        .ok_or_else(|| anyhow!("stack is empty"))?;
                    self.stack.push(val);
                }
                Operation::Store(modifier, optindex) => {
                    let (ptrv, index, val) = match optindex {
                        Some(i) => (self.top_offset(1)?, *i, self.top()?),
                        None => (self.top_offset(2)?, self.top_offset(1)?, self.top()?),
                    };
                    let ptr = ptrv as *mut usize;
                    if !self.allocmgr.has(ptr) {
                        return Err(anyhow!("Tried to deref an unknown pointer"));
                    }
                    unsafe { *(ptr.add(index)) = val };
                    match modifier {
                        Modifier::Preserve => {}
                        Modifier::Destroy => {
                            self.pop()?;
                            self.pop()?;
                            if optindex.is_none() {
                                self.pop()?;
                            }
                        }
                    }
                }
                Operation::Swap => {
                    let v1 = self.pop()?;
                    let v2 = self.pop()?;
                    self.stack.push(v1);
                    self.stack.push(v2);
                }
                Operation::LoadGlobal(s) => {
                    let val = *self
                        .globals
                        .get(s)
                        .ok_or_else(|| anyhow!("Unknown global: `{}`", s))?;
                    self.stack.push(val);
                }
                Operation::StoreGlobal(s, m) => {
                    let val = match m {
                        Modifier::Preserve => self.top()?,
                        Modifier::Destroy => self.pop()?,
                    };
                    let entry = self.globals.get_mut(s).ok_or_else(|| {
                        anyhow!(
                            "Please declare your global variable `{}` in top level declaration",
                            s
                        )
                    })?;
                    *entry = val;
                }
            }
        }
        Ok(())
    }
    fn run_function(&mut self, fnname: &str, args: Vec<usize>) -> anyhow::Result<usize> {
        let f = self
            .interpreter
            .functions
            .get(fnname)
            .ok_or_else(|| anyhow!("Function not found: {}", fnname))?;
        let result = match f {
            FnEnum::Native(f) => f(self, args)?,
            FnEnum::User(ops) => {
                self.run_instructions(ops)?;
                0
            }
        };
        Ok(result)
    }
}

impl ExecutionResult {
    pub fn success(stdout: String) -> Self {
        Self {
            stdout,
            error: None,
        }
    }
    pub fn error(error: String) -> Self {
        Self {
            stdout: String::new(),
            error: Some(error),
        }
    }
}
