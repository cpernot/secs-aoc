use interpreter::ExecutionResult;
use interpreter::Interpreter;

pub mod interpreter;
#[cfg(feature = "timeout")]
pub fn run(source: &str, timeout_ms: u64) -> ExecutionResult {
    let mut i = match Interpreter::new(source) {
        Ok(i) => i,
        Err(e) => return ExecutionResult::error(e.to_string()),
    };
    if timeout_ms > 0 {
        i.run_timeout(timeout_ms)
    } else {
        i.run()
    }
}
#[cfg(not(feature = "timeout"))]
pub fn run(source: &str) -> (String, String) {
    let mut i = match Interpreter::new(source) {
        Ok(i) => i,
        Err(e) => return ExecutionResult::error(e.to_string()),
    };
    i.run
}
