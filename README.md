# Self Explanatory Code Syntax

## Dependencies
* llvm-16

## Usage
* Compile an object file: `secs -c source.sx -o source.o`
* Compile an executable: `secs main.sx -o main`

## Not Yet Implemented:
* Custom optimization level
* Address on stack
